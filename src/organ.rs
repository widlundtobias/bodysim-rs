use dpx_db::table::Table;
use habanero::dren;
use habanero::render::DebugRender;
use rand::{thread_rng, Rng};
use std::collections::HashMap;

pub type OrganId = usize;

#[derive(Debug)]
pub struct OrganConnection {
    pub weight: u8,
}

#[derive(Debug)]
pub struct Organ {
    pub max_size: u32,
    pub contents_count: u32,
    pub connections: HashMap<OrganId, OrganConnection>,
}
impl Organ {
    pub fn new(volume: u32) -> Self {
        Self {
            connections: HashMap::new(),
            contents_count: 0,
            max_size: volume,
        }
    }

    pub fn connect_one_way(&mut self, target_id: OrganId, weight: u8) {
        let connection = self.connections.get(&target_id);
        if connection.is_none() {
            self.connections
                .insert(target_id, OrganConnection { weight });
        }
    }

    pub fn disconnect_one_way(&mut self, target_id: OrganId) {
        self.connections.remove(&target_id);
    }
    pub fn is_full(&self) -> bool {
        self.contents_count >= self.max_size
    }
}

pub type FluidUnitId = usize;

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum FluidType {
    EmptyBloodCell,
    OxygenatedBloodCell,
    CarbonatedBloodCell,
    _Water,
    Glucose,
    _FattyAcid,
}

pub type MilliJoule = i32;
pub const GLUCOSE_ENERGY_CONTENT: MilliJoule = 1_643_000;
pub fn energy_content(t: FluidType) -> MilliJoule {
    match t {
        FluidType::Glucose => GLUCOSE_ENERGY_CONTENT,
        _ => 0,
    }
}

pub struct PumpStateData {
    pub target_size: u32,
    pub time: i32,
    pub speed: i32,
}

#[derive(Copy, Clone)]
pub enum PumpState {
    Idle = 0,
    Contract = 1,
}
pub struct OrganPump {
    pub timer: i32,
    pub state: PumpState,
    pub contraction_energy_cost: MilliJoule,
    pub from_id: OrganId,
    pub to_id: OrganId,
    pub state_data: [PumpStateData; 2],
}
impl OrganPump {
    pub fn new(contraction_energy_cost: i32, from_id: usize, to_id: usize) -> Self {
        Self {
            timer: 0,
            state: PumpState::Contract,
            contraction_energy_cost,
            from_id,
            to_id,
            state_data: [
                PumpStateData {
                    target_size: 2000,
                    time: 26,
                    speed: 75,
                },
                PumpStateData {
                    target_size: 600,
                    time: 10,
                    speed: 150,
                },
            ],
        }
    }
}

pub struct OrganEnergyStorage {
    pub count: MilliJoule,
    pub max: MilliJoule,
}
impl OrganEnergyStorage {
    pub fn new_full(size: MilliJoule) -> Self {
        Self {
            count: size,
            max: size,
        }
    }
    pub fn acquire(&mut self, desired: MilliJoule) -> MilliJoule {
        let missing = -(self.count - desired).min(0);
        let acquired = desired - missing;
        self.count -= acquired;
        acquired
    }
    pub fn acquire_all_or_none(&mut self, desired: MilliJoule) -> Option<MilliJoule> {
        if desired > self.count {
            None
        } else {
            self.count -= desired;
            Some(desired)
        }
    }
}

pub struct OrganOxygenStorage {
    pub count: i32,
    pub max: i32,
}
impl OrganOxygenStorage {
    pub fn new_full(size: i32) -> Self {
        Self {
            count: size,
            max: size,
        }
    }
    pub fn feed(&mut self, amount: i32) -> Option<i32> {
        let space = self.max - self.count;
        let rest = (amount - space).max(0);
        self.count += amount - rest;
        match rest {
            r if r == 0 => None,
            r => Some(r),
        }
    }
}

pub struct OrganEnergyProducer {
    under_conversion: MilliJoule, //this is filled "instantly" up to capacity by reducing energy units in the blood such as glucose. it's energy available for conversion into stored energy
    under_conversion_max: MilliJoule,
    conversion_rate: MilliJoule, //how many units are converted to stored (aka usable) energy per frame;
    conversion_oxygen_supply: MilliJoule, //how many units are converted to stored (aka usable) energy per frame;
}
impl OrganEnergyProducer {
    const CONVERSION_OXYGEN_COST: i32 = 7; //how many frames of conversion that can occur on one oxygen unit
    pub fn new() -> Self {
        Self {
            under_conversion: 0,
            under_conversion_max: 2_000_000,
            conversion_rate: 50,
            conversion_oxygen_supply: 20,
        }
    }
}
pub struct OrganBloodOxygenator {
    pub oxygenation_rate: i32,
}
impl OrganBloodOxygenator {
    const ENERGY_COST: i32 = 2; //millijoule per oxygenated blood
}
pub struct OrganBloodDecarbonator {
    pub decarbonation_rate: i32,
}
impl OrganBloodDecarbonator {
    const ENERGY_COST: i32 = 2; //millijoule per decarbonated blood
}
pub struct OrganPassiveEnergyDrain {}
impl OrganPassiveEnergyDrain {
    const ENERGY_COST: i32 = 7;
}
pub struct OrganAirOxygenCollector {
    pub collect_rate: i32,
}
impl OrganAirOxygenCollector {
    const ENERGY_COST: i32 = 10; //millijoule per frame of collected oxygen
}
#[derive(Copy, Clone, Debug)]
pub struct FluidUnit {
    pub currently_in: OrganId,
    pub fluid_type: FluidType,
    pub id: FluidUnitId,
}

pub struct OrganNodeData {
    pub organs: OrganList,
    pub fluids: Vec<FluidUnit>,
    pub pumps: HashMap<OrganId, OrganPump>,
    pub energy_storages: HashMap<OrganId, OrganEnergyStorage>,
    pub air_oxygen_collectors: HashMap<OrganId, OrganAirOxygenCollector>,
    pub oxygen_storages: HashMap<OrganId, OrganOxygenStorage>,
    pub blood_oxygenator: HashMap<OrganId, OrganBloodOxygenator>,
    pub blood_decarbonator: HashMap<OrganId, OrganBloodDecarbonator>,
    pub passive_energy_drains: HashMap<OrganId, OrganPassiveEnergyDrain>,
    pub energy_producers: HashMap<OrganId, OrganEnergyProducer>,
    pub visual_positions: HashMap<OrganId, glm::Vec2>,
}
#[derive(Debug)]
pub enum TransferErr {
    InputIdNotFound,
    UnitNotInSource,
    TargetFull,
}

impl OrganNodeData {
    pub fn try_transfer_fluid_unit(
        &mut self,
        unit_id: FluidUnitId,
        from_id: OrganId,
        to_id: OrganId,
    ) -> Result<(), TransferErr> {
        let unit = self
            .fluids
            .get_mut(unit_id)
            .ok_or(TransferErr::InputIdNotFound)?;
        &mut self
            .organs
            .get(from_id)
            .ok_or(TransferErr::InputIdNotFound)?;
        &mut self.organs.get(to_id).ok_or(TransferErr::InputIdNotFound)?;

        if unit.currently_in != from_id {
            return Err(TransferErr::UnitNotInSource);
        }
        if self.organs[to_id].is_full() {
            return Err(TransferErr::TargetFull);
        }

        unit.currently_in = to_id;
        self.organs[from_id].contents_count -= 1;
        self.organs[to_id].contents_count += 1;

        Ok(())
    }
}
pub type OrganList = Vec<Organ>;

pub fn connect(list: &mut OrganList, id_a: OrganId, id_b: OrganId, weight: u8) {
    list[id_a].connect_one_way(id_b, weight);
    list[id_b].connect_one_way(id_a, weight);
}
pub fn disconnect(list: &mut OrganList, id_a: OrganId, id_b: OrganId) {
    list[id_a].disconnect_one_way(id_b);
    list[id_b].disconnect_one_way(id_a);
}

pub struct FluidsView<'a> {
    fluids: &'a mut Vec<FluidUnit>,
}
impl<'a> FluidsView<'a> {
    fn has_at_least_in_organ(&self, count: usize, f_type: FluidType, id: OrganId) -> bool {
        self.fluids
            .iter()
            .filter(|u| u.fluid_type == f_type && u.currently_in == id)
            .count()
            >= count
    }

    fn find_unit_of_type_in_organ(
        &self,
        organ_id: OrganId,
        fluid_type: FluidType,
    ) -> Option<FluidUnitId> {
        self.fluids
            .iter()
            .enumerate()
            .find(|u| u.1.currently_in == organ_id && u.1.fluid_type == fluid_type)
            .map(|u| u.0)
    }

    fn change_units_type_in_organ(
        &mut self,
        from_type: FluidType,
        to_type: FluidType,
        count: usize,
        id: OrganId,
    ) -> Result<(), ()> {
        let mut to_convert = count;
        for u in self
            .fluids
            .iter_mut()
            .filter(|u| u.currently_in == id && u.fluid_type == from_type)
        {
            u.fluid_type = to_type;
            to_convert -= 1;
            if to_convert == 0 {
                break;
            }
        }

        if to_convert == 0 {
            Ok(())
        } else {
            Err(())
        }
    }
}

pub struct FluidsOrganView<'a, 'b> {
    fluids: &'a mut Vec<FluidUnit>,
    organs: &'b mut OrganList,
}
impl<'a, 'b> FluidsOrganView<'a, 'b> {
    fn remove_unit(&mut self, unit_id: FluidUnitId) -> Result<(), ()> {
        let organ_id = match self.fluids.get(unit_id) {
            None => return Err(()),
            Some(u) => u.currently_in,
        };

        let organ = self.organs.get_mut(organ_id).ok_or(())?;
        if organ.contents_count == 0 {
            return Err(());
        }

        let last_id = self.fluids.len() - 1;
        self.fluids.swap(unit_id, last_id);
        self.fluids.pop();
        organ.contents_count -= 1;

        Ok(())
    }
}
pub fn update_body(organ_data: &mut OrganNodeData) {
    //shuffle some units to make them not turbo
    let perc_to_shuffle = 0.05;
    let amount_to_shuffle = (organ_data.fluids.len() as f32 * perc_to_shuffle) as usize;

    let mut rng = thread_rng();

    for i in 0..amount_to_shuffle {
        let old_index = organ_data.fluids.len() - 1 - i;
        let new_index = rng.gen_range(0, organ_data.fluids.len() - 1 - amount_to_shuffle);

        let fluids_slice = &mut *organ_data.fluids;
        fluids_slice.swap(old_index, new_index);
    }

    for i in 0..organ_data.fluids.len() {
        let unit = &organ_data.fluids[i];
        let source_id = unit.currently_in;
        let source_organ = &organ_data.organs[source_id];
        let source_contents_count = source_organ.contents_count;
        let source_pressure = source_contents_count as f32 / source_organ.max_size as f32;
        let transference_chances = source_organ
            .connections
            .iter()
            .map(|(&target_id, _)| {
                let target_organ = &organ_data.organs[target_id];
                let target_contents_count = target_organ.contents_count;
                let target_pressure = target_contents_count as f32 / target_organ.max_size as f32;
                let transfer_chance = target_pressure / source_pressure;
                transfer_chance
            })
            .collect::<Vec<f32>>();

        let mut stay_chance = 1.0;
        let mut total_chance = 0.0;

        let mut move_chances = transference_chances
            .iter()
            .map(|tr_chance| {
                stay_chance *= tr_chance;
                let move_chance = (1.0 - tr_chance).min(1.0).max(0.0);
                total_chance += move_chance;
                move_chance
            })
            .collect::<Vec<f32>>();
        move_chances.push(stay_chance);
        total_chance += stay_chance;

        let selected = rng.gen_range(0.0, total_chance);

        let mut accumulator = 0.0;
        let mut selected_i = 0;
        for (i, chance) in move_chances.iter().enumerate() {
            accumulator += chance;
            if accumulator >= selected {
                selected_i = i;
                break;
            }
        }

        if selected_i != move_chances.len() - 1 {
            if let Some((&target_id, _)) = source_organ.connections.iter().nth(selected_i) {
                organ_data
                    .try_transfer_fluid_unit(i as FluidUnitId, source_id, target_id)
                    .unwrap();
            }
        }
    }
}

#[derive(Debug)]
pub enum PumpUpdateErr {
    InvalidThisOrgan,
    NoEnergyStorage,
}

//human baseline on this model is 1J per heart beat
pub fn update_pumps(organ_data: &mut OrganNodeData) -> Result<(), PumpUpdateErr> {
    for (&id, pump) in &mut organ_data.pumps {
        let this_organ = organ_data
            .organs
            .get_mut(id)
            .ok_or(PumpUpdateErr::InvalidThisOrgan)?;
        let energy_storage = organ_data
            .energy_storages
            .get_mut(&id)
            .ok_or(PumpUpdateErr::NoEnergyStorage)?;
        let weight = 6;

        let energy_needed = match pump.state {
            PumpState::Idle => 0,
            PumpState::Contract => pump.contraction_energy_cost,
        };

        if let None = energy_storage.acquire_all_or_none(energy_needed) {
            continue;
        }

        if pump.timer == 0 {
            match pump.state {
                PumpState::Idle => {
                    pump.state = PumpState::Contract;
                    pump.timer = pump.state_data[PumpState::Contract as usize].time;

                    //now contracting - connect to next organ and disconnect previous organ
                    disconnect(&mut organ_data.organs, id, pump.from_id);
                    connect(&mut organ_data.organs, id, pump.to_id, weight);
                }
                PumpState::Contract => {
                    pump.state = PumpState::Idle;
                    pump.timer = pump.state_data[PumpState::Idle as usize].time;

                    //now relaxing - connect previous organ to us and disconnect next organ
                    disconnect(&mut organ_data.organs, id, pump.to_id);
                    connect(&mut organ_data.organs, id, pump.from_id, weight);
                }
            }
        } else {
            let current_state = &pump.state_data[pump.state as usize];
            let target_size = current_state.target_size;
            let speed = current_state.speed;
            let diff = match target_size as i32 - this_organ.max_size as i32 {
                x if x < 0 => -1,
                x if x > 0 => 1,
                _ => 0,
            };

            if diff != 0 {
                this_organ.max_size = (this_organ.max_size as i32 + speed * diff) as u32;
            }
            pump.timer -= 1;
        }
    }

    Ok(())
}
pub fn update_air_oxygen_collector(organ_data: &mut OrganNodeData) {
    let air_oxygen_collectors = &mut organ_data.air_oxygen_collectors;
    let oxygen_storages = &mut organ_data.oxygen_storages;
    let energy_storages = &mut organ_data.energy_storages;

    for (id, oxy_collector) in air_oxygen_collectors.iter_mut() {
        let oxy_storage = match oxygen_storages.get_mut(&id) {
            None => continue,
            Some(x) => x,
        };
        let energy_storage = match energy_storages.get_mut(&id) {
            None => continue,
            Some(x) => x,
        };

        if let Some(_) = energy_storage.acquire_all_or_none(OrganAirOxygenCollector::ENERGY_COST) {
            oxy_storage.feed(oxy_collector.collect_rate);
        }
    }
}
pub fn update_blood_oxygenator(organ_data: &mut OrganNodeData) {
    let blood_oxygenators = &mut organ_data.blood_oxygenator;
    let oxygen_storages = &mut organ_data.oxygen_storages;
    let energy_storages = &mut organ_data.energy_storages;

    for (id, oxygenator) in blood_oxygenators.iter_mut() {
        let oxy_storage = match oxygen_storages.get_mut(&id) {
            None => continue,
            Some(x) => x,
        };
        if oxy_storage.count == 0 {
            continue;
        }
        let energy_storage = match energy_storages.get_mut(&id) {
            None => continue,
            Some(x) => x,
        };

        let mut oxygenation_count = 0;
        for unit in organ_data
            .fluids
            .iter_mut()
            .filter(|u| u.currently_in == *id && u.fluid_type == FluidType::EmptyBloodCell)
        {
            if let Some(_) = energy_storage.acquire_all_or_none(OrganBloodOxygenator::ENERGY_COST) {
                unit.fluid_type = FluidType::OxygenatedBloodCell;
                oxygenation_count += 1;
                oxy_storage.count -= 1;
                if oxygenation_count == oxygenator.oxygenation_rate {
                    break;
                }
            }
        }
    }
}
pub fn update_blood_decarbonator(organ_data: &mut OrganNodeData) {
    let energy_storages = &mut organ_data.energy_storages;

    for (id, decarbonator) in organ_data.blood_decarbonator.iter_mut() {
        let energy_storage = match energy_storages.get_mut(&id) {
            None => continue,
            Some(x) => x,
        };

        let mut decarbonation_count = 0;
        for unit in organ_data
            .fluids
            .iter_mut()
            .filter(|u| u.currently_in == *id && u.fluid_type == FluidType::CarbonatedBloodCell)
        {
            if let Some(_) = energy_storage.acquire_all_or_none(OrganBloodDecarbonator::ENERGY_COST)
            {
                unit.fluid_type = FluidType::OxygenatedBloodCell;
                decarbonation_count += 1;
                if decarbonation_count == decarbonator.decarbonation_rate {
                    break;
                }
            }
        }
    }
}
pub fn update_passive_energy_drain(organ_data: &mut OrganNodeData) {
    let passive_energy_drains = &mut organ_data.passive_energy_drains;
    let energy_storages = &mut organ_data.energy_storages;
    for (id, _) in passive_energy_drains {
        let energy_storage = match energy_storages.get_mut(&id) {
            None => continue,
            Some(x) => x,
        };

        energy_storage.acquire(OrganPassiveEnergyDrain::ENERGY_COST);
    }
}

//1 unit of glucose is 0.4kcal
//1 heartbeat = 1 joule
//1 unit of glucose is 1643 joule
//heart should store ~~500 beats? for now. store in millijoule.
//should use oxygen and blood sugar to create energy storage
pub fn update_energy_producers(organ_data: &mut OrganNodeData) {
    let energy_producers = &mut organ_data.energy_producers;
    let energy_storages = &mut organ_data.energy_storages;

    let fits_between = |v, low, high| low + v <= high;

    for (&id, producer) in energy_producers {
        let energy_storage = match energy_storages.get_mut(&id) {
            None => continue,
            Some(x) => x,
        };

        {
            let f = FluidsView {
                fluids: &mut organ_data.fluids,
            };
            if let Some(glucose_index) = f.find_unit_of_type_in_organ(id, FluidType::Glucose) {
                let energy_amount = energy_content(FluidType::Glucose);
                //if energy molecule fits in underConversion storage, remove it from blood and fill the storage
                if fits_between(
                    energy_amount,
                    producer.under_conversion,
                    producer.under_conversion_max,
                ) {
                    FluidsOrganView {
                        fluids: &mut organ_data.fluids,
                        organs: &mut organ_data.organs,
                    }
                    .remove_unit(glucose_index)
                    .unwrap();
                    producer.under_conversion += energy_amount;
                }
            }
        }
        //as far as underConversion storage allows, move conversionRate from underConversion into energyStorage (if it fits)
        //if no oxygen is present, no conversion may occurr
        if producer.conversion_oxygen_supply == 0 {
            let has_enough_oxygen = FluidsView {
                fluids: &mut organ_data.fluids,
            }
            .has_at_least_in_organ(1, FluidType::OxygenatedBloodCell, id);
            if has_enough_oxygen {
                FluidsView {
                    fluids: &mut organ_data.fluids,
                }
                .change_units_type_in_organ(
                    FluidType::OxygenatedBloodCell,
                    FluidType::CarbonatedBloodCell,
                    1,
                    id,
                )
                .unwrap();
                producer.conversion_oxygen_supply = OrganEnergyProducer::CONVERSION_OXYGEN_COST;
            } else {
                continue;
            }
        }

        //has enough oxy: 0 under conversion: 4929000 conversion rate: 50 energyConversionOxygenCost: 5

        //if such conversion happens, subtract oxygen from the fluid and replace with carbon dioxide
        if producer.under_conversion >= producer.conversion_rate {
            producer.conversion_oxygen_supply -= 1;

            let to_convert =
                (energy_storage.max - energy_storage.count).min(producer.conversion_rate);
            producer.under_conversion -= to_convert;
            energy_storage.count += to_convert;
        }
    }
}

pub fn sprite_size() -> glm::Vec2 {
    glm::vec2(25.0, 25.0)
}

//// update sprites
pub fn update_sprites(
    sprite_t: &mut habanero::render_2d::TSprite,
    world_position_t: &mut habanero::spatial_2d::TWorldPosition2d,
    drawable_color_t: &mut habanero::render::TDrawableColor,
    organ_data: &mut OrganNodeData,
    selection_data: &Vec<FluidUnitId>,
    sprites: &HashMap<OrganId, Option<habanero::entity::EntityId>>,
) {
    #[derive(Copy, Clone, Debug)]
    struct TypeCount {
        r: i32,
        g: i32,
        b: i32,
    }

    let mut type_counts: Vec<TypeCount> = Vec::with_capacity(organ_data.visual_positions.len());
    type_counts.resize(
        organ_data.visual_positions.len(),
        TypeCount { r: 0, g: 0, b: 0 },
    );

    for unit in &organ_data.fluids {
        let type_count = &mut type_counts[unit.currently_in];

        match unit.fluid_type {
            FluidType::EmptyBloodCell => {}
            FluidType::OxygenatedBloodCell => type_count.r += 1,
            FluidType::CarbonatedBloodCell => type_count.b += 1,
            FluidType::Glucose => type_count.g += 1,
            FluidType::_Water => unimplemented!(),
            FluidType::_FattyAcid => unimplemented!(),
        }
    }

    for (organ_id, sprite_id) in sprites
        .iter()
        .filter_map(|e| e.1.map(|entity_id| (*e.0, entity_id)))
    {
        let organ = &organ_data.organs[organ_id];
        let max_size = organ.max_size;
        let type_count = &type_counts[organ_id];

        let r = (type_count.r as f32 / 2000.0) * 4.0 + 0.1;
        let g = (type_count.g as f32 / 2000.0) * 4.0 + 0.1;
        let b = (type_count.b as f32 / 2000.0) * 4.0 + 0.1;

        drawable_color_t.get_mut(sprite_id).unwrap().color =
            habanero::color::Color::opaque((r * 255.0) as u8, (g * 255.0) as u8, (b * 255.0) as u8);

        let size_factor = max_size as f32 / 2000.0;
        let sprite = sprite_t.get_mut(sprite_id).unwrap();
        sprite.scale = sprite_size() * size_factor;
    }

    //selection visuals

    let mut organ_counters: Vec<u32> = Vec::with_capacity(organ_data.visual_positions.len());
    organ_counters.resize(organ_data.visual_positions.len(), 0);

    for selected in organ_data
        .fluids
        .iter()
        .filter(|&f| selection_data.contains(&f.id))
    {
        let sprite_id = sprites[&selected.currently_in].unwrap();

        let pos = world_position_t.get(sprite_id).unwrap().coord;

        let x = organ_counters[selected.currently_in] as f32 * 4.0;

        use habanero::render::DRect;

        dren!(:color: habanero::color::brown(),
        DRect::new_2d(
            glm::vec2(pos.x - 28.0 + x, pos.y - 20.0),
            glm::vec2(2.0, 2.0)
        ),);

        organ_counters[selected.currently_in] += 1;
    }

    //connections
    for (&from_organ_id, sprite_id) in sprites.iter().filter_map(|e| e.1.map(|o| (e.0, o))) {
        let from_organ = &organ_data.organs[from_organ_id];
        let from_pos = world_position_t.get(sprite_id).unwrap().coord;

        for (&to_organ_id, _) in &from_organ.connections {
            let &to_sprite_id = &sprites[&to_organ_id].unwrap();

            let to_pos = world_position_t.get(to_sprite_id).unwrap().coord;

            let dir = glm::normalize(&(to_pos - from_pos));
            let side_dir = glm::rotate_vec2(&dir, glm::pi::<f32>() / 2.0);

            let side_offset = 12.0;

            let from = from_pos + side_dir * side_offset + dir * 5.0;
            let to = to_pos + side_dir * side_offset - dir * 5.0;

            use habanero::render::DArrow;

            dren!(
                :color: habanero::color::orange(),
                :depth: 0.5,
                DArrow::new_2d(from,  to, 13.0),
            );
        }
    }
}

pub fn update_simple(
    heart_enabled: bool,
    lung_enabled: bool,
    oxygen_enabled: bool,
    organ_data: &mut OrganNodeData,
    sprite_t: &mut habanero::render_2d::TSprite,
    world_position_2d_t: &mut habanero::spatial_2d::TWorldPosition2d,
    drawable_color_t: &mut habanero::render::TDrawableColor,
    selected_units: &Vec<FluidUnitId>,
    organ_sprite_map: &HashMap<OrganId, Option<habanero::entity::EntityId>>,
) {
    update_body(organ_data);
    if heart_enabled {
        update_pumps(organ_data).unwrap();
    }
    if lung_enabled {
        update_air_oxygen_collector(organ_data);
        update_blood_oxygenator(organ_data);
        update_blood_decarbonator(organ_data);
    }
    if oxygen_enabled {
        update_energy_producers(organ_data);
        update_passive_energy_drain(organ_data);
    }
    update_sprites(
        sprite_t,
        world_position_2d_t,
        drawable_color_t,
        organ_data,
        &selected_units,
        &organ_sprite_map,
    );
}
