use dpx_db::table::AutoId;
use habanero::render::RendererId;

pub const QUAD_RENDERER: RendererId =
    RendererId::new(unsafe { std::num::NonZeroUsize::new_unchecked(1) });
pub const DEBUG_RENDERER: RendererId =
    RendererId::new(unsafe { std::num::NonZeroUsize::new_unchecked(2) });

pub struct Graphics {
    pub render_settings: habanero::render::RenderSettings,
    pub shader: habanero::render::ShaderId,
    pub quad_renderer: habanero::render::RendererId,
    pub debug_renderer: habanero::render::RendererId,
}

impl Graphics {
    pub fn new(
        window_size: glm::UVec2,
        texture_t: &mut habanero::render::TTexture,
        shader_t: &mut habanero::render::TShader,
        renderer_t: &mut habanero::render::TRenderer,
        viewport_t: &mut habanero::render::TViewport,
        camera_t: &mut habanero::render::TCamera,
        render_pass_t: &mut habanero::render::TRenderPass,
        gl: std::rc::Rc<habanero::gl::Gl>,
    ) -> Self {
        //rendering settings
        let render_settings = habanero::render::RenderSettings::new(texture_t, gl.clone());

        //we only need one shader and this is the one
        let shader = shader_t
            .insert(habanero::render_2d::sprite_shader::new_sprite_shader(
                gl.clone(),
            ))
            .id;

        //set up our renderers
        let quad_renderer = habanero::render::add_renderer(
            QUAD_RENDERER,
            renderer_t,
            habanero::render::QuadRenderer::new_with_builders(
                vec![Box::new(habanero::render_2d::SpriteQuadBuilder::new())],
                gl.clone(),
            ),
        )
        .id;
        let debug_renderer = habanero::render::add_renderer(
            DEBUG_RENDERER,
            renderer_t,
            habanero::render::DebugRenderer::new(gl),
        )
        .id;

        //viewports and camera
        let viewport = viewport_t
            .insert(habanero::render::Viewport {
                start: glm::vec2(0, 0),
                size: window_size,
                clear_color: habanero::color::Color::opaque(100, 100, 100),
            })
            .id;
        let camera = camera_t
            .insert(habanero::render::Camera::new(
                habanero::render::Projection::new_orthographic(
                    glm::vec2(window_size.y as f32, window_size.x as f32),
                    1.0,
                    -100.0,
                    100.0,
                ),
            ))
            .id;

        //configure render passes
        render_pass_t.insert(habanero::render::RenderPass {
            order: 5,
            viewport,
            renderer: quad_renderer,
            camera,
        });
        render_pass_t.insert(habanero::render::RenderPass {
            order: 4,
            viewport,
            renderer: debug_renderer,
            camera,
        });

        Self {
            render_settings,
            shader,
            quad_renderer,
            debug_renderer,
        }
    }
}
