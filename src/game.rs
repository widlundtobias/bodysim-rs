use dpx_db::table::Table;
use habanero::entity::EntityId;
use maplit::hashmap;
use std::collections::HashMap;

use crate::db;
use crate::graphics;
use crate::organ;
use crate::ugly;
use crate::ui;

pub struct Game {
    window: habanero::window::WindowSystem,
    db: db::DbAll,
    input: habanero::input::InputSystem,
    graphics: graphics::Graphics,
    ui: ui::Ui,
    organ_data: organ::OrganNodeData,
    organ_sprite_map: HashMap<organ::OrganId, Option<EntityId>>,
}

impl Game {
    pub fn new() -> anyhow::Result<Self> {
        let window_size = glm::vec2(800, 600);

        let window = habanero::window::WindowSystem::new(
            "Body Simulator 2020",
            window_size,
            habanero::window::GlProfile::ES3,
        )?;

        let mut db = db::DbAll::new();

        let input = habanero::input::InputSystem::new(&window.sdl);

        let graphics = graphics::Graphics::new(
            window_size,
            &mut db.hab_render.texture_t,
            &mut db.hab_render.shader_t,
            &mut db.hab_render.renderer_t,
            &mut db.hab_render.viewport_t,
            &mut db.hab_render.camera_t,
            &mut db.hab_render.render_pass_t,
            window.gl.clone(),
        );
        let organ_data = ugly::create_ugly();

        let mut res = Self {
            window,
            db,
            input,
            graphics,
            ui: ui::Ui::new(),
            organ_data,
            organ_sprite_map: HashMap::<organ::OrganId, Option<EntityId>>::new(),
        };

        crate::scenario::test_scenario(
            &mut res.db,
            &mut res.organ_data,
            &mut res.organ_sprite_map,
            res.graphics.shader,
        );

        Ok(res)
    }
}

impl habanero::app::MainLoop for Game {
    fn main_loop(&mut self) -> habanero::app::MainLoopEvent {
        let mut returned = habanero::app::MainLoopEvent::Continue;

        //////MARK NEW FRAME
        ////Rendering
        habanero::render::frame_start(&self.db.hab_render);

        //////INPUT
        let input = self.input.update_input_state(&self.window.sdl);

        ////quit message
        if input.system_quit.is_some()
            || input
                .started_key_presses
                .contains(&sdl2::keyboard::Scancode::Q)
        {
            returned = habanero::app::MainLoopEvent::Quit;
        }

        ////handle resize message
        if let Some(resized) = input.window_resized {
            let x = resized.x;
            let y = resized.y;

            habanero::render::adjust_viewports_with(
                |v, index, count| {
                    let x_inc = (x as usize / count) as u32;
                    let index = index as u32;
                    let y = y as u32;

                    v.start = glm::vec2(index * x_inc, 0);
                    v.size = glm::vec2(x_inc, y);
                },
                &mut self.db.hab_render.viewport_t,
            );

            let cam = self.db.hab_render.camera_t.first_mut().unwrap().data;
            cam.translation = glm::vec3(x as f32 / 2.0, y as f32 / 2.0, 0.0);

            cam.projection = habanero::render::Projection::new_orthographic(
                glm::vec2(x as f32, y as f32),
                1.0,
                -100.0,
                100.0,
            )
        }

        ////selection
        self.ui.update_selection(
            &input,
            &self.db.hab_spatial_2d.world_position_2d_t,
            &self.db.hab_render_2d.sprite_t,
            &self.organ_data,
            &self.organ_sprite_map,
        );

        //////LOGIC
        ////spatial tree
        habanero::spatial_2d::update_world_spatial(&mut self.db.hab_spatial_2d);

        ////organs
        let heart_enabled = true;
        let lung_enabled = true;
        let oxygen_enabled = true;
        organ::update_simple(
            heart_enabled,
            lung_enabled,
            oxygen_enabled,
            &mut self.organ_data,
            &mut self.db.hab_render_2d.sprite_t,
            &mut self.db.hab_spatial_2d.world_position_2d_t,
            &mut self.db.hab_render.drawable_color_t,
            &self.ui.selected_units,
            &self.organ_sprite_map,
        );

        //////RENDERING
        ////dispatchers
        let sprite_quad_dispatcher = habanero::render_2d::SpriteQuadBuilderDispatcher {
            tables: (
                &self.db.hab_spatial_2d.world_position_2d_t,
                &self.db.hab_spatial_2d.world_rotation_2d_t,
                &self.db.hab_spatial_2d.world_depth_t,
                &self.db.hab_render_2d.sprite_t,
                &self.db.hab_render.drawable_shader_t,
                &self.db.hab_render.drawable_color_t,
                &self.db.hab_render_2d.sub_rect_sprite_t,
                &self.db.hab_render_2d.animated_sprite_t,
                &self.db.hab_render.shader_t,
                &self.db.hab_render.texture_t,
                &self.db.hab_render_2d.sprite_animation_t,
            ),
        };

        let quad_dispatcher = habanero::render::QuadRendererDispatcher {
            builder_dispatchers: &hashmap! {
            habanero::render_2d::SPRITE_QUAD_BUILDER_ID => & sprite_quad_dispatcher as &dyn habanero::render::QuadBuilderDispatcher,
            },
        };
        let debug_dispatcher = habanero::render::DebugRendererDispatcher {
            shader: self.graphics.shader,
        };

        ////use dispatchers to render the frame
        habanero::render::render_frame(
            hashmap! {
                self.graphics.quad_renderer => &quad_dispatcher as &dyn habanero::render::RendererDispatcher,
                self.graphics.debug_renderer => &debug_dispatcher as &dyn habanero::render::RendererDispatcher,
            },
            &self.graphics.render_settings,
            &self.db.hab_render.render_pass_t,
            &self.db.hab_render.renderer_t,
            &self.db.hab_render.viewport_t,
            &self.db.hab_render.camera_t,
            &self.db.hab_render.texture_t,
            &self.db.hab_render.shader_t,
            &mut self.db.hab_render.uniform_location_t,
            &self.window.gl,
        )
        .unwrap();

        ////swap window buffers
        self.window.window.gl_swap_window();

        returned
    }
}
