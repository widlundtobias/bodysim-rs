use habanero::entity::{EntityId, EntityProperties};
use habanero::hash::TypeHash;
use rand::seq::SliceRandom;
use rand::thread_rng;
use std::collections::HashMap;

pub fn test_scenario(
    db_all: &mut crate::db::DbAll,
    organ_data: &mut crate::organ::OrganNodeData,
    sprites: &mut HashMap<crate::organ::OrganId, Option<EntityId>>,
    sprite_shader_id: habanero::render::ShaderId,
) {
    let main_pos = glm::vec2(100.0, 100.0);
    let depth = 0.5;
    ////create sprite entities for all organs
    let mut spawn_sprite = |index, position| {
        let mut props = habanero::render_2d::add_sprite_properties(
            EntityProperties::new(),
            main_pos + position,
            depth,
            None,
            None,
            Some(crate::organ::sprite_size()),
            None,
            sprite_shader_id,
        );

        habanero::add_property!(
            props,
            habanero::render::DrawableColor {
                color: habanero::color::white()
            }
        );

        let id = db_all.next_id();
        habanero::entity::insert_from_props(id, db_all, &props);

        sprites.insert(index, Some(id));
    };

    ////fill each organ with fluids
    let mut next_fluid_id = 0;
    for (organ_id, organ) in organ_data.organs.iter_mut().enumerate() {
        //blod cells
        let unit_count = 1600;
        organ_data
            .fluids
            .resize_with(organ_data.fluids.len() + unit_count, || {
                let new_id = next_fluid_id;
                next_fluid_id += 1;

                crate::organ::FluidUnit {
                    fluid_type: crate::organ::FluidType::EmptyBloodCell,
                    currently_in: organ_id,
                    id: new_id, // assigned after this loop
                }
            });

        organ.contents_count = unit_count as u32;
        //glucose
        let glucose_count = 2; //27 == fully eaten
        organ_data.fluids.resize(
            organ_data.fluids.len() + glucose_count,
            crate::organ::FluidUnit {
                fluid_type: crate::organ::FluidType::Glucose,
                currently_in: organ_id,
                id: next_fluid_id, // assigned after this loop
            },
        );
        organ.contents_count += glucose_count as u32;

        let factor = 5.0;
        spawn_sprite(
            organ_id,
            organ_data.visual_positions.get(&organ_id).unwrap() * factor,
        );
    }

    organ_data.fluids.as_mut_slice().shuffle(&mut thread_rng());
}
