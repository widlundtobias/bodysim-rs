extern crate nalgebra_glm as glm;
extern crate sdl2;

mod db;
mod game;
mod graphics;
mod organ;
mod scenario;
mod ugly;
mod ui;

fn main() {
    let game = game::Game::new().unwrap();

    habanero::app::run(game);
}
