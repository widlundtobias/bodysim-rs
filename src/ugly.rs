use super::organ::*;
use maplit::hashmap;

pub const HEART_ID: OrganId = 0;
pub const LUNGS_ID: OrganId = 1;
pub const PANCREAS_ID: OrganId = 2;
pub const LIVER_ID: OrganId = 3;
pub const INTESTINES_ID: OrganId = 4;
pub const SPLEEN_ID: OrganId = 5;
pub const LEFT_LEG_ID: OrganId = 6;
pub const RIGHT_LEG_ID: OrganId = 7;
pub const LEFT_ARM_ID: OrganId = 8;
pub const RIGHT_ARM_ID: OrganId = 9;
pub const HEAD_ID: OrganId = 10;
pub const HEART_VEIN_ID: OrganId = 11;
pub const DIGESTIVE_VEIN_ID: OrganId = 12;
pub const EXTREMITIES_VEIN_ID: OrganId = 13;
pub const LUNGS_VEIN_ID: OrganId = 14;

pub fn create_ugly() -> OrganNodeData {
    let mut res = OrganNodeData {
        organs: vec![
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
            Organ::new(2000),
        ],
        fluids: Vec::new(),
        pumps: hashmap! {HEART_ID => OrganPump::new(91, LUNGS_ID, HEART_VEIN_ID)},
        energy_storages: hashmap! {//stores in millijoule
            HEART_ID => OrganEnergyStorage::new_full(5 * 1000),
            LUNGS_ID            => OrganEnergyStorage::new_full(3 * 1000),
            PANCREAS_ID         => OrganEnergyStorage::new_full(3 * 1000),
            LIVER_ID            => OrganEnergyStorage::new_full(3 * 1000),
            INTESTINES_ID       => OrganEnergyStorage::new_full(3 * 1000),
            SPLEEN_ID           => OrganEnergyStorage::new_full(3 * 1000),
            LEFT_LEG_ID         => OrganEnergyStorage::new_full(3 * 1000),
            RIGHT_LEG_ID        => OrganEnergyStorage::new_full(3 * 1000),
            LEFT_ARM_ID         => OrganEnergyStorage::new_full(3 * 1000),
            RIGHT_ARM_ID        => OrganEnergyStorage::new_full(3 * 1000),
            HEAD_ID             => OrganEnergyStorage::new_full(3 * 1000),
            HEART_VEIN_ID       => OrganEnergyStorage::new_full(3 * 1000),
            DIGESTIVE_VEIN_ID   => OrganEnergyStorage::new_full(3 * 1000),
            EXTREMITIES_VEIN_ID => OrganEnergyStorage::new_full(3 * 1000),
            LUNGS_VEIN_ID       => OrganEnergyStorage::new_full(3 * 1000),
        },
        air_oxygen_collectors: hashmap! {LUNGS_ID => OrganAirOxygenCollector{collect_rate: 3}},
        oxygen_storages: hashmap! {LUNGS_ID => OrganOxygenStorage::new_full(500)},
        blood_oxygenator: hashmap! {LUNGS_ID => OrganBloodOxygenator{oxygenation_rate:3}},
        blood_decarbonator: hashmap! {LUNGS_ID => OrganBloodDecarbonator{decarbonation_rate:3}},
        passive_energy_drains: hashmap! {
            HEART_ID => OrganPassiveEnergyDrain{},
            LUNGS_ID            => OrganPassiveEnergyDrain{},
            PANCREAS_ID         => OrganPassiveEnergyDrain{},
            LIVER_ID            => OrganPassiveEnergyDrain{},
            INTESTINES_ID       => OrganPassiveEnergyDrain{},
            SPLEEN_ID           => OrganPassiveEnergyDrain{},
            LEFT_LEG_ID         => OrganPassiveEnergyDrain{},
            RIGHT_LEG_ID        => OrganPassiveEnergyDrain{},
            LEFT_ARM_ID         => OrganPassiveEnergyDrain{},
            RIGHT_ARM_ID        => OrganPassiveEnergyDrain{},
            HEAD_ID             => OrganPassiveEnergyDrain{},
            HEART_VEIN_ID       => OrganPassiveEnergyDrain{},
            DIGESTIVE_VEIN_ID   => OrganPassiveEnergyDrain{},
            EXTREMITIES_VEIN_ID => OrganPassiveEnergyDrain{},
            LUNGS_VEIN_ID       => OrganPassiveEnergyDrain{},
        },
        energy_producers: hashmap! {
            HEART_ID => OrganEnergyProducer::new(),
            LUNGS_ID            => OrganEnergyProducer::new(),
            PANCREAS_ID         => OrganEnergyProducer::new(),
            LIVER_ID            => OrganEnergyProducer::new(),
            INTESTINES_ID       => OrganEnergyProducer::new(),
            SPLEEN_ID           => OrganEnergyProducer::new(),
            LEFT_LEG_ID         => OrganEnergyProducer::new(),
            RIGHT_LEG_ID        => OrganEnergyProducer::new(),
            LEFT_ARM_ID         => OrganEnergyProducer::new(),
            RIGHT_ARM_ID        => OrganEnergyProducer::new(),
            HEAD_ID             => OrganEnergyProducer::new(),
            HEART_VEIN_ID       => OrganEnergyProducer::new(),
            DIGESTIVE_VEIN_ID   => OrganEnergyProducer::new(),
            EXTREMITIES_VEIN_ID => OrganEnergyProducer::new(),
            LUNGS_VEIN_ID       => OrganEnergyProducer::new(),
        },
        visual_positions: hashmap! {
            HEART_ID            => glm::vec2(50.0, 10.0),
            LUNGS_ID            => glm::vec2(40.0, 20.0),
            PANCREAS_ID         => glm::vec2(70.0, 30.0),
            LIVER_ID            => glm::vec2(80.0, 40.0),
            INTESTINES_ID       => glm::vec2(80.0, 30.0),
            SPLEEN_ID           => glm::vec2(90.0, 30.0),
            LEFT_LEG_ID         => glm::vec2(40.0, 70.0),
            RIGHT_LEG_ID        => glm::vec2(60.0, 70.0),
            LEFT_ARM_ID         => glm::vec2( 0.0, 60.0),
            RIGHT_ARM_ID        => glm::vec2(30.0, 60.0),
            HEAD_ID             => glm::vec2(10.0, 20.0),
            HEART_VEIN_ID       => glm::vec2(60.0, 20.0),
            DIGESTIVE_VEIN_ID   => glm::vec2(80.0, 20.0),
            EXTREMITIES_VEIN_ID => glm::vec2(30.0, 30.0),
            LUNGS_VEIN_ID       => glm::vec2(40.0, 40.0),
        },
    };

    let mut do_connect = |a, b| {
        let weight = 6;
        connect(&mut res.organs, a, b, weight);
    };

    //heart->heart vein
    do_connect(HEART_ID, HEART_VEIN_ID);
    //heart vein -> other veins
    do_connect(HEART_VEIN_ID, DIGESTIVE_VEIN_ID);
    do_connect(HEART_VEIN_ID, EXTREMITIES_VEIN_ID);

    //digestive vein -> digestive stuff
    do_connect(DIGESTIVE_VEIN_ID, PANCREAS_ID);
    do_connect(DIGESTIVE_VEIN_ID, INTESTINES_ID);
    do_connect(DIGESTIVE_VEIN_ID, SPLEEN_ID);

    //digestive/etc->liver
    do_connect(PANCREAS_ID, LIVER_ID);
    do_connect(INTESTINES_ID, LIVER_ID);
    do_connect(SPLEEN_ID, LIVER_ID);

    //liver -> LungsVein
    do_connect(LIVER_ID, LUNGS_VEIN_ID);

    //extremities vein -> extremities
    do_connect(EXTREMITIES_VEIN_ID, HEAD_ID);
    do_connect(EXTREMITIES_VEIN_ID, LEFT_ARM_ID);
    do_connect(EXTREMITIES_VEIN_ID, RIGHT_LEG_ID);
    do_connect(EXTREMITIES_VEIN_ID, LEFT_LEG_ID);
    do_connect(EXTREMITIES_VEIN_ID, RIGHT_ARM_ID);

    //extremities -> lungs vein
    do_connect(HEAD_ID, LUNGS_VEIN_ID);
    do_connect(LEFT_ARM_ID, LUNGS_VEIN_ID);
    do_connect(RIGHT_LEG_ID, LUNGS_VEIN_ID);
    do_connect(LEFT_LEG_ID, LUNGS_VEIN_ID);
    do_connect(RIGHT_ARM_ID, LUNGS_VEIN_ID);

    //lungs vein -> lungs
    do_connect(LUNGS_VEIN_ID, LUNGS_ID);

    //lungs -> heart
    do_connect(LUNGS_ID, HEART_ID);

    res
}
