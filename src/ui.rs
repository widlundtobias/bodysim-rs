use habanero::entity::EntityId;
use rand::seq::SliceRandom;
use rand::thread_rng;
use std::collections::HashMap;

pub struct Ui {
    pub hovered_organ: Option<crate::organ::OrganId>,
    pub selected_units: Vec<crate::organ::FluidUnitId>,
}

impl Ui {
    pub fn new() -> Self {
        Self {
            hovered_organ: None,
            selected_units: Vec::new(),
        }
    }

    pub fn update_selection(
        &mut self,
        input: &habanero::input::RawInput,
        world_position_2d_t: &habanero::spatial_2d::TWorldPosition2d,
        sprite_t: &habanero::render_2d::TSprite,
        organ_data: &crate::organ::OrganNodeData,
        organ_sprite_map: &HashMap<crate::organ::OrganId, Option<EntityId>>,
    ) {
        let mouse_pos: glm::Vec2 = glm::convert(input.cursor_position);

        dpx_macros::join!(
            join_iter = JoinEntry,
            world_position_2d_t: pos,
            sprite_t: sprite
        );

        for JoinEntry { id, pos, sprite } in join_iter {
            let half_scale = sprite.scale / 2.0;
            let start = pos.coord - half_scale;
            let end = start + sprite.scale;

            if mouse_pos.x > start.x
                && mouse_pos.y > start.y
                && mouse_pos.x < end.x
                && mouse_pos.y < end.y
            {
                for (organ_id, sprite_id) in organ_sprite_map {
                    if Some(id) == *sprite_id {
                        self.hovered_organ = Some(*organ_id);
                        break;
                    }
                }
            }
        }

        if input
            .started_mouse_presses
            .contains(&sdl2::mouse::MouseButton::Left)
        {
            if let Some(hovered_id) = self.hovered_organ {
                let canditates: Vec<_> = organ_data
                    .fluids
                    .iter()
                    .filter_map(|u| {
                        if u.currently_in == hovered_id {
                            Some(u.id)
                        } else {
                            None
                        }
                    })
                    .collect();

                if !canditates.is_empty() {
                    self.selected_units.clear();
                    self.selected_units.resize_with(40, || {
                        *canditates.as_slice().choose(&mut thread_rng()).unwrap()
                    });
                }
            }
        }
    }
}
